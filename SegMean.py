import numpy as np
import matplotlib.pyplot as plt
import os
import h5py

from myErrors import *
from myFunctions import *
from SegregationPP import *

plt.ion()

class SegMean(SegregationPP):

    def __init__(self, simu=None, path=path):
        """
    path : str, reference path where all simulations are stored
    simu : str, name of the folder in which the simulation to load is located
    """
        SegregationPP.__init__(self, simu, path)

        self.H = 1
        self.T = 1
        self.hdf5File = "average.hdf5"
        self.load_GeomParam()
        self.load_hdf5()
        self.compute_dynamics()
        self.find_quasiStatic()

    def load_data(self, varName):

        loadFile = h5py.File(self.directory+self.hdf5File)
        try:
            self.__setattr__(varName, loadFile[varName].value)
        except NameError:
            print("Variable {} does not exist in {}.".format(varName,self.directory+self.hdf5File))

    def load_hdf5(self):
        loadFile = h5py.File(self.directory+self.hdf5File)
        variables = loadFile.keys()
        for varName in variables:
            self.__setattr__(varName, loadFile[varName].value)
            print("Variable {} has been loaded.".format(varName))
        self.time = self.time/self.T
        self.zAxis = self.zAxis/self.H
        self.massCenter = self.massCenter/self.H
        self.ndimz = len(self.zAxis)
        self.dz = self.fluidHeight/(self.ndimz-1)

    def phiPart_fit(self, plot=True):
        self.z_phiGauss = np.linspace(-5*self.diameterPart1/self.H,5*self.diameterPart1/self.H,1000)
        self.meanPhiB /= self.H
        try:
            self.phiGauss = self.meanPhiA*np.exp(-self.z_phiGauss**2/(2*self.meanPhiB**2))
        except AttributeError:
            print("No data for the concentration in fine particles")
        if plot:
            plt.figure()
            plt.plot(self.phiGauss, self.z_phiGauss)

        #Equivalent step
        self.phiStep = np.zeros(len(self.z_phiGauss))
        for i in range(len(self.z_phiGauss)):
            if abs(self.z_phiGauss[i]) < self.Nlayer2*self.diameterPart2/self.H:
                self.phiStep[i] = self.meanPhiA*self.meanPhiB*(2*np.pi)**(0.5)/(2*self.Nlayer2*self.diameterPart2/self.H)
        if plot:
            plt.figure()
            plt.plot(self.phiGauss, self.z_phiGauss, linewidth=3)
            plt.plot(self.phiStep, self.z_phiGauss, linewidth=3)
            plt.xlabel(r'$\phi^s$', fontsize=30)
            plt.ylabel(r'$\bar{z}-\bar{z}_c$', rotation=True, horizontalalignment='right', fontsize=40)


    def compute_dynamics(self):
        self.shearRate = derivate(self.zAxis, self.vxPartMean)/self.H
        self.shearRateF = interp1d(self.zAxis, self.shearRate)
        self.dzdt = derivate(self.time, self.massCenter)*self.H

    def adim_variables(self, H=None, adim_time=False, plot=False):

        if H is None :
            self.H = self.diameterPart1
        else:
            self.H = H
        if adim_time is True:
            self.T = np.sqrt(self.H/9.81)
        else:
            self.T = 1.

        self.load_hdf5()
        self.compute_dynamics()
        self.find_quasiStatic()
        self.vxPart_fit(plot=plot)
        self.ZmassCenter(plot=plot)
        self.compute_I()
        self.segregation_flux(plot=plot)
        self.phiPart_fit(plot=plot)

