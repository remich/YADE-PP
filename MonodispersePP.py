import numpy as np
import matplotlib.pyplot as plt
import sys
import h5py
from SegregationPP import *
from TimeData import *

plt.ion()

class MonodispersePP(SegregationPP):

    def __init__(self, simu=None, path=path):
        SegregationPP.__init__(self, simu, path)

    def load_GeomParam(self):
        myFile = self.directory+'GeomParam.txt'
        with open(myFile,'r') as f:
            for lines in f:
                if lines.startswith('Nlayer'):
                    self.Nlayer = float(lines.split(' ')[-1].split('\n')[0])
                if lines.startswith('diameterPart'):
                    self.diameterPart = float(lines.split(' ')[-1].split('\n')[0])
                if lines.startswith('waterDepth'):
                    self.waterDepth = eval(lines.split(' ')[-1].split('\n')[0])
                if lines.startswith('slope'):
                    self.slope = float(lines.split(' ')[-1].split('\n')[0])
                if lines.startswith('measurePeriod'):
                     self.measurePeriod = float(lines.split(' ')[-1].split('\n')[0])

        self.fluidHeight = self.Nlayer*self.diameterPart + self.waterDepth
        self.rhoPart = 2500.
        self.rhoFluid = 1000.
 
    def axis(self):

        self.time = np.linspace(self.firstTimeStepLoaded * self.measurePeriod,
                                self.lastTimeStepLoaded * self.measurePeriod, len(self.data)) / self.T
        self.ndimz = len(self.data[0].var["phiPart"])
        self.dz = self.fluidHeight / (self.ndimz - 1)
        self.zAxis = np.linspace(0, self.fluidHeight / self.H, self.ndimz)

    def particle_averaged(self):
        vxPartMean = np.zeros(self.ndimz)
        vxFluid = np.zeros(self.ndimz)
        phiPartMean = np.zeros(self.ndimz)
        qs = np.zeros(len(self.data))

        for i in range(len(self.data)):
            phiPartMean += self.data[i].var["phiPart"]
            vxPartMean += self.data[i].var["phiPart"]*self.data[i].var["vxPart"]
            self.data[i].var["shearRate"] = derivate(self.zAxis, self.data[i].var["vxPart"])
            if len(self.data[i].var['vxFluid'])==len(self.data[i].var['phiPart']):
                vxFluid += (1-self.data[i].var["phiPart"])*self.data[i].var["vxFluid"]
            else:
                vxFluid += (1-self.data[i].var["phiPart"])*self.data[i].var["vxFluid"][:-1]
            qs[i] = self.dz*sum(self.data[i].var["vxPart"]*self.data[i].var["phiPart"])/ \
        (9.81*np.cos(self.slope)*(self.rhoPart/self.rhoFluid-1)*self.diameterPart**3)**(0.5)

        nonZeroIndices = np.where(phiPartMean>0)
        vxPartMean[nonZeroIndices] = vxPartMean[nonZeroIndices]/phiPartMean[nonZeroIndices]
        self.vxPartMean = vxPartMean
        self.phiPartMean = phiPartMean/len(self.data)
        vxFluid/=(len(self.time)*(1-self.phiPartMean))
        self.vxFluid = vxFluid
        self.qs = qs
        self.shearStress = derivate(self.zAxis, self.vxPartMean)

    def compute_I(self):
        self.IMean = np.zeros(len(self.zAxis))
        index = np.where(self.PMean>1e-6)[0]
        self.IMean[index] = self.diameterPart * self.shearRate[index] / np.sqrt(self.PMean[index] / self.rhoPart)
        self.IMeanF = interp1d(self.zAxis, self.IMean)

    def compute_dynamics(self):
        self.shearRate = derivate(self.zAxis, self.vxPartMean)
        self.shearRateF = interp1d(self.zAxis, self.shearRate)
 
    def find_quasiStatic(self, crit=0.2):
        """
        Find the lower and upper bound of the quasi static part, based on the analysis of vxPartMean
        """
        index = np.where(self.vxPartMean>0.0)
        vx = self.vxPartMean[index]
        z = self.zAxis[index]
        lnvx = np.log(vx)
        dlnvx = derivate(z,lnvx)

        #Compute the index at half height
        middleIndex = int(find_zero(range(len(z)),z-(z[-1]+z[0])/2))
        middleSlope = dlnvx[middleIndex]

        upperBound = middleIndex
        lowerBound = middleIndex
        while (abs(dlnvx[upperBound]-middleSlope) < crit*middleSlope):# and (dlnvx[upperBound]<1.0):
            upperBound +=1
        while (abs(dlnvx[lowerBound]-middleSlope) < crit*middleSlope):# and (dlnvx[lowerBound]<1.0):
            lowerBound -=1

        self.quasiStaticLowerBound = z[lowerBound]
        self.quasiStaticUpperBound = z[upperBound]
        self.zQS = z[lowerBound:upperBound+1]

    def vxPart_fit(self, zStart=None, zEnd=None, plot=True):
        """
        Fit the mean total particle velocity.
        Vx = a*exp(bz)
        ln(Vx) = ln(a) + bz
        coefVx = [a, b]
        """
        if zStart==None:
            zStart = max(self.zAxis[0], self.quasiStaticLowerBound)
        else:
            zStart = max(self.zAxis[0], zStart)
        if zEnd==None:
            zEnd = min(self.zAxis[-1], self.quasiStaticUpperBound)
        else:
            zEnd = min(self.zAxis[-1], zEnd)

        index = np.where((self.zAxis>=zStart)&(self.zAxis<=zEnd))[0]
        if self.zQS[0]!= self.zAxis[index][0] or self.zQS[-1]!=self.zAxis[index][-1]:
            print("Warning : the zone corresponding to the quasi static part is being modified.")
            self.modify_quasiStatic(index)

        lnVx = np.log(self.vxPartMean[index])
        [b, lna] = np.polyfit(self.zAxis[index], lnVx, 1)
        self.coefVx = [np.exp(lna), b]

        self.zQS = self.zAxis[index]
        self.vxQS = self.coefVx[0]*np.exp(self.coefVx[1]*self.zQS)
        self.gamma = self.coefVx[0]*self.coefVx[1]*np.exp(self.coefVx[1]*self.zQS)

        if plot is True:
            linewidth = 3
            plt.figure()
            plt.subplot(121)
            plt.plot(self.vxPartMean, self.zAxis, linewidth = linewidth)
            plt.plot(self.vxQS, self.zQS, 'r--',
                label=r'${:.1e} \times exp({:.3f}z)$'.format(self.coefVx[0], self.coefVx[1]),
                linewidth=linewidth)
            plt.legend()
            plt.xlabel(r'$\left<V_x\right>$ $(m.s^{-1})$', fontsize=18)
            plt.ylabel(r'$\frac{z}{d_1}$', rotation=True, horizontalalignment='right', fontsize=24)
            plt.ylim([self.zAxis[0], self.zAxis[-1]])

            plt.subplot(122)
            plt.semilogx(self.vxPartMean, self.zAxis, linewidth = linewidth)
            plt.semilogx(self.vxQS, self.zQS, 'r--',
                label=r'${:.3f} + {:.3f} z$'.format(np.log(self.coefVx[0]), self.coefVx[1]),
                linewidth=linewidth)
            plt.legend()
            plt.grid()
            plt.xlabel(r'$\left<V_x\right>$ $(m.s^{-1})$', fontsize=18)
            plt.ylabel(r'$\frac{z}{d_1}$', rotation=True, horizontalalignment='right', fontsize=24)
            plt.ylim([self.zAxis[0], self.zAxis[-1]])

    def save_average_values(self, fileName='average.hdf5', overwrite=0):
        self.write_data("time", fileName, overwrite=overwrite)
        self.write_data("qs", fileName, overwrite=overwrite)
        self.write_data("zAxis", fileName, overwrite=overwrite)
        self.write_data("vxPartMean", fileName, overwrite=overwrite)
        self.write_data("phiPartMean", fileName, overwrite=overwrite)
        self.write_data("etaMean", fileName, overwrite=overwrite)
        self.write_data("muMean", fileName, overwrite=overwrite)
        self.write_data("tauMean", fileName, overwrite=overwrite)
        self.write_data("PMean", fileName, overwrite=overwrite)
