import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import h5py
import sys
from SegregationPP import *
from TimeData import *
from time import time

plt.ion()

class SegFull(SegregationPP):

    def __init__(self, simu=None, path=path, start=0, end=None, step=None):
        """
	path : str, reference path where all simulations are stored
	simu : str, name of the folder in which the simulation to load is located
	start : int, first time step to load
    	end : int, last time step to load. If None takes the last time step of the .hdf5 file
    	step : int, step of loading. If None, take an appropriate one
	"""
        SegregationPP.__init__(self, simu, path)

        # self.hdf5File = find_hdf5_file(self.path, self.simu)
        self.H = 1
        self.T = 1
        self.hdf5File = "data.hdf5"
        self.load_hdf5(start=start, end=end, step=step)
        self.load_GeomParam()
        self.axis()
        self.create_phiPartGran()
        self.particle_averaged()
        self.compute_dynamics()
        self.find_quasiStatic()
        #self.load_velocity()
        #self.percolation_velocity()

    def adim_variables(self, H=None, adim_time=False, plot=False):
        if H is None:
            self.H = self.diameterPart1
        else:
            self.H = H
        if adim_time is True:
            self.T = np.sqrt(self.H / 9.81)
        else:
            self.T = 1.

        self.axis()
        self.compute_dynamics()
        self.find_quasiStatic()
        self.compute_I()
        self.vxPart_fit(plot=plot)
        self.ZmassCenter(plot=plot)

    def load_hdf5(self, start=0, end=None, step=None):
        """
	    create as many TimeData objects as time steps stored in the hdf5 file
	    start : int, first time step to load
	    end : int, last time step to load, if None, load until the last time step
	    step : int, step of loading. If None, take an appropriate one
	    """
        loadFile = h5py.File(self.directory + self.hdf5File)
        # The keys in hdf5 files are not sorted making difficult to find the first and last time
        # step. Trick to find these values.
        n = loadFile.keys()
        for i in range(len(n)):
            n[i] = int(n[i])
        n.sort()
        firstTimeStep = int(n[0])
        lastTimeStep = int(n[-1])
        self.firstTimeSimu = firstTimeStep
        self.lastTimeSimu = lastTimeStep

        if start < firstTimeStep:
            print("Warning : start={} is smaller than the first time step recorded in the hdf5 file." + \
                  "start is set to the first time step".format(start))
            start = firstTimeStep

        if end == None:
            end = lastTimeStep
        elif end > lastTimeStep:
            print("Warning : end={} is bigger than the last time step recorded in the hdf5 file." + \
                  "end is set to the last time step.".format(end))
            end = lastTimeStep

        self.firstTimeStepLoaded = start
        self.lastTimeStepLoaded = end

        if step == None:
            step = 1
        self.step = step

        self.data = []
        for i in range(start, end + 1, step):
            print('Loading time step {}\r'.format(i)),
            sys.stdout.flush()
            self.data.append(TimeData(i, directory=self.directory, hdf5File=self.hdf5File))

    def load_velocity(self, width=30):
        self.velFile = 'velocity.hdf5'
        start = self.firstTimeStepLoaded
        end = self.lastTimeStepLoaded

        groundPosition = 5. / 4 * self.fluidHeight

        self.particlesData = []
        for i in range(start, end + 1, self.step):
            print("Loading time step {}\r".format(i)),
            sys.stdout.flush()
            self.particlesData.append(TimeData(i, directory=self.directory, hdf5File=self.velFile))
            self.particlesData[-1].var["positionPartZ"] -= groundPosition

        # Define the type of particles:
        # 0 : the fixed part at the bottom
        # 1 : large Particles
        # 2 : fine Particles

        phiPartMax = 0.61
        Vpart1 = np.pi / 6 * (self.diameterPart1) ** 3
        Vpart2 = np.pi / 6 * (self.diameterPart2) ** 3
        S = (width * self.diameterPart1) ** 2
        self.partNumber1 = int(self.Nlayer1 * phiPartMax * self.diameterPart1 * S / Vpart1)
        self.partNumber2 = int(self.Nlayer2 * phiPartMax * self.diameterPart2 * S / Vpart2)

        self.particlesType = np.zeros(len(self.particlesData[0].var["positionPartZ"]))
        self.particlesType[len(self.particlesType) - self.partNumber1 - self.partNumber2:len(self.particlesType) - self.partNumber2] = 1
        self.particlesType[len(self.particlesType) - self.partNumber2:] = 2

    def load_pressure(self):
        PMean = np.zeros(len(self.zAxis))
        for i in range(len(self.time)):
            stressFile = self.directory+'stress/'+str(i)+'.py'
            print(stressFile)
            execfile(stressFile)
            P = np.zeros(len(self.zAxis))
            for j in range(len(self.zAxis)):
                P[j] = -1./3*(stressProfile[0][j][0]+stressProfile[0][j][4]+stressProfile[0][j][8])

            self.data[i].var["P"] = P
            PMean += self.data[i].var["phiPart"]*P
        index = np.where(self.phiPart>0)
        PMean[index] /= self.phiPart[index]

    def extract_stress(self):
        PMean = np.zeros(len(self.zAxis))
        PMean1 = np.zeros(len(self.zAxis))
        PMean2 = np.zeros(len(self.zAxis))
        tauMean = np.zeros(len(self.zAxis))
        tauMean1 = np.zeros(len(self.zAxis))
        tauMean2 = np.zeros(len(self.zAxis))
        muMean = np.zeros(len(self.zAxis))
        etaMean = np.zeros(len(self.zAxis))
        for t in range(len(self.time)):
            P1 = np.zeros(len(self.zAxis))
            P2 = np.zeros(len(self.zAxis))
            tau1 = np.zeros(len(self.zAxis))
            tau2 = np.zeros(len(self.zAxis))
            #eta = np.zeros(len(self.zAxis))
            mu = np.zeros(len(self.zAxis))
            for i in range(len(self.zAxis)):
                P1[i] = - self.data[t].var["stress1"][i][8]
                P2[i] = - self.data[t].var["stress2"][i][8]
                tau1[i] = self.data[t].var["stress1"][i][2]
                tau2[i] = self.data[t].var["stress2"][i][2]
            self.data[t].var["P1"] = P1
            self.data[t].var["P2"] = P2
            self.data[t].var["P"] = P1+P2
            self.data[t].var["tau1"] = tau1
            self.data[t].var["tau2"] = tau2
            self.data[t].var["tau"] = tau1+tau2
            index = np.where((P1+P2)>1e-6)[0]
            mu[index] = (tau1[index]+tau2[index])/(P1[index]+P2[index])
            self.data[t].var["mu"] = mu
            #index = np.where(self.data[t].var["shearRate"]>1e-6)[0]
            #eta[index] = (tau1[index]+tau2[index])/self.data[t].var["shearRate"][index]
            #self.data[t].var["eta"] = eta
            PMean += self.data[t].var["phiPart"]*(P1+P2)
            PMean1 += self.data[t].var["phiPart1"]*P1
            PMean2 += self.data[t].var["phiPart2"]*P2
            tauMean += self.data[t].var["phiPart"]*(tau1+tau2)
            tauMean1 += self.data[t].var["phiPart1"]*tau1
            tauMean2 += self.data[t].var["phiPart2"]*tau2
            #etaMean += self.data[t].var["phiPart"]*eta

        index = np.where(self.phiPartMean>0)[0]
        PMean[index]/=((len(self.time)*self.phiPartMean[index]))
        tauMean[index]/=((len(self.time)*self.phiPartMean[index]))
        index = np.where(self.phiPartMean1>0)[0]
        PMean1[index]/=((len(self.time)*self.phiPartMean1[index]))
        tauMean1[index]/=((len(self.time)*self.phiPartMean1[index]))
        index = np.where(self.phiPartMean2>0)[0]
        PMean2[index]/=((len(self.time)*self.phiPartMean2[index]))
        tauMean2[index]/=((len(self.time)*self.phiPartMean2[index]))
        index = np.where(self.shearRate>0)[0]
        #etaMean[index]/=((len(self.time)*self.phiPartMean[index]))
        etaMean[index] = tauMean[index]/self.shearRate[index]
        self.PMean = PMean
        self.PMean1 = PMean1
        self.PMean2 = PMean2
        self.tauMean = tauMean
        self.tauMean1 = tauMean1
        self.tauMean2 = tauMean2
        self.etaMean = etaMean
        index = np.where(PMean>1e-6)[0]
        muMean[index] = tauMean[index]/PMean[index]
        self.muMean = muMean

    def segregation_force(self, plot=True):
        index = np.where((self.zAxis>=3.5)&(self.zAxis<9))[0]
        dzdtBot = (-self.coefMassCenter[0]*
                np.exp(-(self.zAxis[index]+self.Nlayer2*self.diameterPart2/self.H-self.coefMassCenter[1])/self.coefMassCenter[0]))
        Sr = dzdtBot
        self.Fmu = 18*self.etaMean[index]*Sr/(self.diameterPart1*np.sqrt(9.81*self.diameterPart1)*(self.rhoPart-self.rhoFluid)*0.61**2)


        index = np.where((self.zAxis>=3.5)&(self.zAxis<9))[0]
        Sr2 = self.Sr0*self.IMean[index]**self.powerI
        self.Fmu2 = 18*self.etaMean[index]*Sr2/(self.diameterPart1*np.sqrt(9.81*self.diameterPart1)*(self.rhoPart-self.rhoFluid)*0.61)

        if plot is True:
            plt.figure()
            plt.plot(self.muMean[index], self.Fmu)
            plt.plot(self.muMean[index], Fmu2)

    def axis(self):

        self.time = np.linspace(self.firstTimeStepLoaded * self.measurePeriod,
                                self.lastTimeStepLoaded * self.measurePeriod, len(self.data)) / self.T
        self.ndimz = len(self.data[0].var["phiPart"])
        self.dz = self.fluidHeight / (self.ndimz - 1)
        self.zAxis = np.linspace(0, self.fluidHeight / self.H, self.ndimz)

    def create_phiPartGran(self):
        """
        Compute the volume fraction of large and small particles per unit granular volume.
        """
        self.phiPartGran1 = np.zeros(len(self.zAxis))
        self.phiPartGran2 = np.zeros(len(self.zAxis))
        for i in range(len(self.data)):
            phiPartGran1 = 1.*self.data[i].var["phiPart1"]
            phiPartGran2 = 1.*self.data[i].var["phiPart2"]
            index = np.where(self.data[i].var["phiPart"] > 0)
            phiPartGran1[index] /= self.data[i].var["phiPart"][index]
            phiPartGran2[index] /= self.data[i].var["phiPart"][index]
            self.data[i].var["phiPartGran1"] = phiPartGran1
            self.data[i].var["phiPartGran2"] = phiPartGran2
            self.phiPartGran1 += phiPartGran1
            self.phiPartGran2 += phiPartGran2
        self.phiPartGran1 /= len(self.time)
        self.phiPartGran2 /= len(self.time)

    def particle_averaged(self):
        """
        - Compute the some mean values related to the particles (small and large together) :
	    - total particles velocity : vxPartTot (not averaged).
	    - mean particles velocity : vxPartMean (size ndimz).
	    - mean particles concentration : phiPartMean (size ndimz).
	    - mean solid shear stress
	    """
        vxPartMean = np.zeros(self.ndimz)
        vzPartMean = np.zeros(self.ndimz)
        vxFluid = np.zeros(self.ndimz)
        turbStress = np.zeros(self.ndimz)
        vxPartMean1 = np.zeros(self.ndimz)
        vzPartMean1 = np.zeros(self.ndimz)
        vxPartMean2 = np.zeros(self.ndimz)
        vzPartMean2 = np.zeros(self.ndimz)
        phiPartMean = np.zeros(self.ndimz)
        phiPartMean1 = np.zeros(self.ndimz)
        phiPartMean2 = np.zeros(self.ndimz)
        qs = np.zeros(len(self.data))

        for i in range(len(self.data)):
            self.data[i].var["vxPartTot"] = (self.data[i].var["vxPart1"] * self.data[i].var["phiPartGran1"] + \
                                             self.data[i].var["vxPart2"] * self.data[i].var["phiPartGran2"])
            phiPartMean += (self.data[i].var["phiPart1"] + self.data[i].var["phiPart2"])
            phiPartMean1 += self.data[i].var["phiPart1"]
            phiPartMean2 += self.data[i].var["phiPart2"]
            vxPartMean1 += self.data[i].var["phiPart1"] * self.data[i].var["vxPart1"]
            vxPartMean2 += self.data[i].var["phiPart2"] * self.data[i].var["vxPart2"]
            vzPartMean1 += self.data[i].var["phiPart1"] * self.data[i].var["vzPart1"]
            vzPartMean2 += self.data[i].var["phiPart2"] * self.data[i].var["vzPart2"]
            vxPartMean += (self.data[i].var["phiPart1"] + self.data[i].var["phiPart2"]) * self.data[i].var["vxPartTot"]
            vzPartMean += (self.data[i].var["phiPart1"] + self.data[i].var["phiPart2"]) * self.data[i].var["vzPart"]
            vxFluid += (1-self.data[i].var["phiPart"])*self.data[i].var["vxFluid"][1:]
            turbStress += (1-self.data[i].var["phiPart"])*self.data[i].var["turbStress"]
            self.data[i].var["shearRate"] = derivate(self.zAxis, self.data[i].var["vxPartTot"]) / self.H
            qs[i] = self.dz * sum(self.data[i].var["vxPartTot"] * self.data[i].var["phiPart"]) / \
                    (9.81 * np.cos(self.slope) * (self.rhoPart / self.rhoFluid - 1) * self.diameterPart1 ** 3) ** (0.5)

        nonZeroIndices = np.where(phiPartMean > 0)
        vxPartMean[nonZeroIndices] = vxPartMean[nonZeroIndices] / phiPartMean[nonZeroIndices]
        vzPartMean[nonZeroIndices] = vzPartMean[nonZeroIndices] / phiPartMean[nonZeroIndices]
        nonZeroIndices = np.where(phiPartMean1 > 0)
        vxPartMean1[nonZeroIndices] = vxPartMean1[nonZeroIndices] / phiPartMean1[nonZeroIndices]
        vzPartMean1[nonZeroIndices] = vzPartMean1[nonZeroIndices] / phiPartMean1[nonZeroIndices]
        nonZeroIndices = np.where(phiPartMean2 > 0)
        vxPartMean2[nonZeroIndices] = vxPartMean2[nonZeroIndices] / phiPartMean2[nonZeroIndices]
        vzPartMean2[nonZeroIndices] = vzPartMean2[nonZeroIndices] / phiPartMean2[nonZeroIndices]
        vxFluid /= (len(self.data)-phiPartMean)
        turbStress /= (len(self.data)-phiPartMean)
        self.vxPartMean = vxPartMean
        self.vxPartMean1 = vxPartMean1
        self.vxPartMean2 = vxPartMean2
        self.vzPartMean = vzPartMean
        self.vzPartMean1 = vzPartMean1
        self.vzPartMean2 = vzPartMean2
        self.vxFluid = vxFluid
        self.turbStress = turbStress
        self.phiPartMean = phiPartMean / len(self.data)
        self.phiPartMean1 = phiPartMean1 / len(self.data)
        self.phiPartMean2 = phiPartMean2 / len(self.data)
        self.qs = qs

    def compute_dynamics(self):
        self.shearRate = derivate(self.zAxis, self.vxPartMean) / self.H
        self.shearRateF = interp1d(self.zAxis, self.shearRate)

        # Compute the position of the center of mass for each time
        self.massCenter = np.zeros(len(self.time))
        for i in range(len(self.massCenter)):
            self.massCenter[i] = sum(self.data[i].var["phiPart2"] * self.zAxis) / sum(self.data[i].var["phiPart2"])

    def fluc_vel(self):
        """
        Compute the vertical profile of velocity fluctuations
        """
        self.flucVel = np.zeros(self.ndimz)
        for i in range(len(self.data)):
            self.flucVel += np.abs(self.data[i].var["phiPart"] * (self.data[i].var["vxPartTot"] - self.vxPartMean))
        nonZeroIndices = np.where(self.phiPartMean > 0)
        self.flucVel[nonZeroIndices] = self.flucVel[nonZeroIndices] / (
                len(self.data) * self.phiPartMean[nonZeroIndices])

    def compute_I_old(self):
        """
        Compute the inertial number, for each time step then make the mean
        """
        IMean = np.zeros(self.ndimz)
        for i in range(len(self.data)):
            # Compute mean diameter as a function of z
            self.data[i].var["diameterMean"] = self.data[i].var["phiPartGran1"] * self.diameterPart1 + \
                                               self.data[i].var["phiPartGran2"] * self.diameterPart2
            # Compute the vertical pressure
            tau_zz = np.zeros(len(self.zAxis))
            for j in range(len(self.zAxis)):
                tau_zz[j] = (self.rhoPart - self.rhoFluid) * 9.81 * np.cos(self.slope) * sum(
                    self.data[i].var["phiPart"][j:]) * self.dz
            self.data[i].var["tau_zz"] = tau_zz
            self.data[i].var["I"] = self.data[i].var["shearRate"] * self.data[i].var["diameterMean"] / \
                                    (self.data[i].var["tau_zz"] / self.rhoPart) ** (0.5)
            IMean += self.data[i].var["I"]

        IMean /= len(self.time)
        self.IMean = IMean

    def check_mass(self):
        """
	    Compute the mass of small particles (from phiPart2) and compare it with with its theoretical
	    value (from the number of particles)
	    """
        phiPartMax = 0.61
        Vpart = np.pi / 6 * (self.diameterPart2) ** 3
        rho = 2500
        S = (15 * self.diameterPart1) ** 2
        partNumber = int(self.Nlayer2 * phiPartMax * self.diameterPart2 * S / Vpart)
        theoreticalMass = (partNumber * rho * Vpart) * np.ones(len(self.data))
        computedMass = np.zeros(len(self.data))
        for i in range(len(computedMass)):
            computedMass[i] = rho * S * self.dz * sum(self.data[i].var["phiPart2"])

        plt.figure()
        plt.subplot(2, 1, 1)
        plt.plot(self.time, computedMass, label='Computed mass')
        plt.plot(self.time, theoreticalMass, label='Theoretical mass')
        plt.xlabel('Time (s)')
        plt.ylabel('Mass (kg)')
        plt.legend()
        plt.subplot(2, 1, 2)
        plt.plot(self.time, abs(computedMass - theoreticalMass) / theoreticalMass)
        plt.xlabel('Time (s)')
        plt.ylabel('Error (%)')

        self.computedMass = computedMass
        self.theoreticalMass = theoreticalMass

    def shields_number(self, plot=True):
        """
        Compute the Shields number at each time:
            Sh = rhof * tauf_max / (deltarho*dmix*g*cos(slope))
            tauf_max: maximal value of the turbulent fluid shear stress
            rhof: fluid density
            deltarho: rhop - rhof
            dmix: mean diameter at the surface (phi^p < 0.8*0.61)
        Create self.shieldsNumber: array of the same size than self.time which
        contains the value of the shields number at each time.
        """
        phiPartMax = 0.61
        shieldsNumber = np.zeros(len(self.data))
        for i in range(len(self.data)):
            index = np.where(self.data[i].var["phiPart"] < 0.8 * phiPartMax)
            phi1Sum = np.sum(self.data[i].var["phiPart1"][index])
            phi2Sum = np.sum(self.data[i].var["phiPart2"][index])
            self.diameterMix = (self.diameterPart1 * phi1Sum + self.diameterPart2 * phi2Sum) / (phi1Sum + phi2Sum)

            shieldsNumber[i] = self.rhoFluid * np.max(self.data[i].var["turbStress"]) / (
                    (self.rhoPart - self.rhoFluid) * self.diameterMix * 9.81 * np.cos(self.slope))

        self.shieldsNumber = shieldsNumber
        if plot is True:
            plt.figure()
            plt.plot(self.time, self.shieldsNumber)

    def phiPart_fit(self, plot=True):
        '''
        phi2 = a*exp(-z^2/(2*b^2))
        a = max(phi2)
        int(phi2) = a*b*sqrt(2*pi) = N2*d2*0.61
        '''
        self.coef_phi_a = []
        self.coef_phi_b = []
        for i in range(len(self.data)):
            self.coef_phi_a.append(np.max(self.data[i].var["phiPart2"]))
            self.coef_phi_b.append(
                self.Nlayer2 * self.diameterPart2 / self.H * 0.61 / (np.sqrt(2 * np.pi) * self.coef_phi_a[-1]))
        self.meanPhiA = np.mean(np.array(self.coef_phi_a))
        self.meanPhiB = np.mean(np.array(self.coef_phi_b))

        self.z_phiGauss = np.linspace(-5 * self.diameterPart1 / self.H, 5 * self.diameterPart1 / self.H, 1000)
        self.phiGauss = self.meanPhiA * np.exp(-self.z_phiGauss ** 2 / (2 * self.meanPhiB ** 2))
        if plot:
            plt.figure()
            plt.plot(self.phiGauss, self.z_phiGauss)

        # Equivalent step
        self.phiStep = np.zeros(len(self.z_phiGauss))
        for i in range(len(self.z_phiGauss)):
            if abs(self.z_phiGauss[i]) < self.Nlayer2 * self.diameterPart2 / self.H:
                self.phiStep[i] = self.meanPhiA * self.meanPhiB * (2 * np.pi) ** (0.5) / (
                        2 * self.Nlayer2 * self.diameterPart2 / self.H)
        if plot:
            plt.figure()
            plt.plot(self.phiGauss, self.z_phiGauss, linewidth=3)
            plt.plot(self.phiStep, self.z_phiGauss, linewidth=3)
            plt.xlabel(r'$\phi^s$', fontsize=30)
            plt.ylabel(r'$\bar{z}-\bar{z}_c$', rotation=True, horizontalalignment='right', fontsize=40)

    def phiPartGran_gaussian(self):
        self.coef_phi_a = []
        self.coef_phi_z0 = []
        self.coef_phi_b = []
        for i in range(len(self.data)):
            coef1, coef2 = scipy.optimize.curve_fit(lambda z, a, b: a*np.exp(-z ** 2 / (2 * b ** 2)),
                                                    self.zAxis - self.massCenter[i],
                                                    self.data[i].var["phiPartGran2"])
            self.coef_phi_a.append(coef1[0])
            self.coef_phi_b.append(coef1[1])

        self.mean_coefPhiA = np.mean(np.array(self.coef_phi_a))
        self.mean_coefPhiB = np.mean(np.array(self.coef_phi_b))

        self.z_phiGauss = np.linspace(-5 * self.diameterPart1 / self.H, 5 * self.diameterPart1 / self.H, 1000)
        self.phiGauss = self.mean_coefPhiA * np.exp(-(self.z_phiGauss) ** 2 / (2 * self.mean_coefPhiB ** 2))

        # Equivalent step
        #self.phiStep = np.zeros(len(self.z_phiGauss))
        #for i in range(len(self.z_phiGauss)):
        #    if abs(self.z_phiGauss[i]) < max(self.Nlayer2 * self.diameterPart2 / self.H,
        #                                     self.diameterPart2 / (2 * self.H)):
        #        self.phiStep[i] = self.mean_coefPhiA * self.mean_coefPhiB * (2 * np.pi) ** (0.5) / (
        #            max(2 * self.Nlayer2 * self.diameterPart2 / self.H, self.diameterPart2 / self.H))
        plt.figure()
        plt.plot(self.phiGauss, self.z_phiGauss, linewidth=3)
        #plt.plot(self.phiStep, self.z_phiGauss, linewidth=3)
        plt.xlabel(r'$\phi^s$', fontsize=30)
        plt.ylabel(r'$\bar{z}-\bar{z}_c$', rotation=True, horizontalalignment='right', fontsize=40)

    def fluctuations(self, particles='small', width=30):
        '''
        Compute the velocity fluctuations of particles:
        - particles : - 'moving' for all moving particles
                    - 'small' for small particles
                    - 'large' for large particles
        - width : width of the box (in diameter of large particles)
        '''
        Vcell = (width*self.diameterPart1) ** 2 * self.dz
        self.vzFlucMean = np.zeros(len(self.zAxis))
        # Time loop for time mean
        for t in range(len(self.time)):
            print("time step {}\r".format(t)),
            sys.stdout.flush()
            # Spatial mean
            vzFluc = np.zeros(len(self.zAxis))
            if particles=='moving':
                movingPart = np.where(self.particlesType > 0)[0]
            elif particles=='large':
                movingPart = np.where(self.particlesType == 1)[0]
            elif particles=='small':
                movingPart = np.where(self.particlesType == 2)[0]

            for part in movingPart:
                if self.particlesType[part] == 2:
                    R = self.diameterPart2 / 2
                else:
                    R = self.diameterPart1 / 2
                posZ = self.particlesData[t].var["positionPartZ"][part]
                velZ = self.particlesData[t].var["velocityPartZ"][part]
                I = np.where(abs(self.zAxis - posZ/self.H) < (R +
                                                              self.dz)/self.H)[0]
                for z in I:
                    vzFluc[z] += Vslice(self.zAxis[z] * self.H - self.dz / 2,
                                        self.zAxis[z] * self.H + self.dz / 2, posZ, R) / Vcell * \
                                     (velZ - self.vzPartMean2[z]) ** 2

            index = np.where(self.data[t].var["phiPart2"] > 0)
            vzFluc[index] = vzFluc[index] / self.data[t].var["phiPart2"][index]

            self.data[t].var["vzFluc"] = vzFluc
            self.vzFlucMean += vzFluc

        self.vzFlucMean = np.sqrt(self.vzFlucMean / len(self.time))

    def percolation_velocity(self):
        Vcell = (30 * self.diameterPart1) ** 2 * self.dz
        self.percVel = np.zeros(len(self.zAxis))
        self.phiSmall = np.zeros(len(self.zAxis))
        smallPart = np.where(self.particlesType == 2)[0]
        # Time loop for time mean
        for t in range(len(self.time)):
            print("time step {}\r".format(t)),
            sys.stdout.flush()
            percVel_temp = np.zeros(len(self.zAxis))
            phiSmall_temp = np.zeros(len(self.zAxis))
            for part in smallPart:
                R = self.diameterPart2/2
                posZ = self.particlesData[t].var["positionPartZ"][part]
                velZ = self.particlesData[t].var["velocityPartZ"][part]
                I = np.where(abs(self.zAxis - posZ/self.H) < (R +
                                                              self.dz)/self.H)[0]
                #t1 = time()
                for z in I:#range(len(self.zAxis)):
                    #if abs(self.zAxis[z] - posZ/self.H) < (R + self.dz)/self.H:
                    #t2 = time()
                    percVel_temp[z] += Vslice(self.zAxis[z]-(self.dz/self.H)/2,
                                              self.zAxis[z]+(self.dz/self.H)/2,posZ,R)/Vcell * velZ
                    phiSmall_temp[z] += Vslice(self.zAxis[z]-(self.dz/self.H)/2,
                                              self.zAxis[z]+(self.dz/self.H)/2,posZ,R)/Vcell
                    #t3 = time()
                    #print('Vslice {}'.format(t3-t2))
                #t4 = time()
                #print('1 particle : {}'.format(t4-t1))
            #index = np.where(self.data[t].var["phiPart2"]>0)
            index = np.where(phiSmall_temp>0)
            percVel_temp[index] /= self.data[t].var["phiPart2"][index]
            self.data[t].var["percVel"] = percVel_temp
            self.percVel += percVel_temp
            self.phiSmall += phiSmall_temp

        self.percVel = self.percVel/len(self.time)
        self.phiSmall = self.phiSmall/len(self.time)

    def vertVel_fluctuations(self, width=30):
        Vcell = (width * self.diameterPart1) ** 2 * self.dz
        self.vertVelFluc = np.zeros(len(self.zAxis))
        smallPart = np.where(self.particlesType == 2)[0]
        # Time loop for time mean
        for t in range(len(self.time)):
            print("time step {}\r".format(t)),
            sys.stdout.flush()
            percVel_temp = np.zeros(len(self.zAxis))
            for part in smallPart:
                R = self.diameterPart2/2
                posZ = self.particlesData[t].var["positionPartZ"][part]
                velZ = self.particlesData[t].var["velocityPartZ"][part]
                I = np.where(abs(self.zAxis - posZ/self.H) < (R +
                                                              self.dz)/self.H)[0]
                #t1 = time()
                for z in I:#range(len(self.zAxis)):
                    #if abs(self.zAxis[z] - posZ/self.H) < (R + self.dz)/self.H:
                    #t2 = time()
                    percVel_temp[z] += Vslice(self.zAxis[z]-(self.dz/self.H)/2,
                                              self.zAxis[z]+(self.dz/self.H)/2,posZ,R)/Vcell * velZ**2
                    #t3 = time()
                    #print('Vslice {}'.format(t3-t2))
                #t4 = time()
                #print('1 particle : {}'.format(t4-t1))
            index = np.where(self.data[t].var["phiPart2"]>0)
            percVel_temp[index] /= self.data[t].var["phiPart2"][index]
            self.vertVelFluc += percVel_temp

        self.vertVelFluc = np.sqrt(self.vertVelFluc/len(self.time))

    def flux(self, phi_min=0.01, phi_max=1, dphi=0.01):
        self.phiFlux = np.linspace(phi_min, phi_max, (phi_max-phi_min)/dphi+1)
        self.wFlux = np.zeros(len(self.phiFlux))
        self.n = np.zeros(len(self.phiFlux))
        for t in range(len(self.time)):
            for j in range(len(self.phiFlux)):
                I = np.where((self.data[t].var["phiPartGran2"]-self.phiFlux[j]>0)&(self.data[t].var["phiPartGran2"]-self.phiFlux[j]<0.01))[0]
                self.wFlux[j] += sum(self.data[t].var["percVel"][I]/self.IMean[I]**(0.81))
                self.n[j] += len(I)
        self.wFlux/=self.n

        plt.figure()
        plt.plot((1-self.phiFlux), self.wFlux, 'k')
        plt.xlabel(r'$1-\phi^s$', fontsize=25)
        plt.ylabel(r'$\frac{w}{I^0.81}$', fontsize=25, rotation=True,
                   horizontalalignment='right')

    def map_phi(self, cmap='RdYlGn'):
        phi = []
        for i in range(len(self.data)):
            phi.append(self.data[i].var["phiPartGran2"])
        fig = plt.figure()
        levels = np.linspace(0, 1, 31)  # min(np.max(1.2*phi[-1]),1.),101)
        plt.contourf(self.time, self.zAxis, np.transpose(phi), levels,
                     cmap=cmap)
        cbar = plt.colorbar()
        cbar.set_ticks(np.linspace(0,1,11))
        cbar.set_label(r'$\phi_s$', rotation=True, fontsize=25, verticalalignment='center', horizontalalignment='left', y=0.5)
        if self.top == 0:
            plt.ylim([0, (self.Nlayer1 * self.diameterPart1 + self.Nlayer2 * self.diameterPart2) / self.H])
        else:
            plt.ylim([-(self.Nlayer1 * self.diameterPart1 + self.Nlayer2 * self.diameterPart2) / self.H, 0])
        plt.xlabel(r'$t$', fontsize=25)
        plt.ylabel(r'$z$', fontsize=25, rotation=True, horizontalalignment='right')


    def travelling_wave(self, cmap='RdYlGn'):
        phi = []
        for i in range(len(self.data[1:])):
            phi.append(self.data[i].var["phiPartGran2"])
        fig = plt.figure()
        levels = np.linspace(0, 1, 31)  # min(np.max(1.2*phi[-1]),1.),101)
        ksi = []
        myT = []
        for i in range(len(self.time[1:])):
            ksi.append(self.zAxis+self.coefI[1]/0.81*np.log(self.time[i]))
            myT.append(self.time[i]*np.ones(len(self.zAxis)))
        plt.contourf(myT, ksi, phi, levels, cmap=cmap)
        cbar = plt.colorbar()
        cbar.set_ticks(np.linspace(0,1,11))
        cbar.set_label(r'$\phi_s$', rotation=True, fontsize=25, verticalalignment='center', horizontalalignment='left', y=0.5)
        plt.xlabel(r'$\tau$', fontsize=25)
        plt.ylabel(r'$\xi$', fontsize=25, rotation=True, horizontalalignment='right')
        plt.xlim([np.min(myT), np.max(myT)])
        plt.ylim([0, 25])

    def phi_vid(self, height=1, step=1):
        plt.ioff()
        phi = np.empty((len(self.time), len(self.zAxis),))
        for i in range(len(self.data)):
            phi[i,:] = self.data[i].var["phiPartGran2"]

        fig = plt.figure()
        levels = np.linspace(0,1,101)
        plt.contourf(self.time[:2], self.zAxis, np.transpose(phi[:2]), levels,
                    cmap='RdYlGn')
        plt.ylim([0,height])
        plt.xlim([0,1500])
        plt.xlabel(r'$t$', fontsize=30)
        plt.ylabel(r'$z$', fontsize=30, rotation=True, horizontalalignment='right')
        plt.colorbar()
        plt.savefig('1.png', bbox_inches='tight')
        plt.close('all')
        j=2
        for i in range(2,len(self.data), step):
            plt.figure()
            plt.contourf(self.time[:i+1], self.zAxis, np.transpose(phi[:i+1]), levels,
                         cmap='RdYlGn')
            plt.ylim([0,height])
            plt.xlim([0, self.time[-1]])
            plt.xlabel(r'$t$', fontsize=30)
            plt.ylabel(r'$z$', fontsize=30, rotation=True, horizontalalignment='right')
            plt.colorbar()
            plt.savefig(str(j)+'.png', bbox_inches='tight')
            j+=1
            plt.close('all')
        plt.ion()

    def keys(self):
        return self.data[0].var.keys()

    def momentum_balance(self):
        self.shearStress = np.zeros(self.ndimz)
        self.Rxz = np.zeros(self.ndimz)
        self.gravityTerm = np.zeros(self.ndimz)
        for i in range(len(self.data)):
            for j in range(len(self.zAxis)):
                self.shearStress[j] += self.data[i].var["stress"][j][2]
            self.Rxz += self.rhoFluid*self.data[i].var["turbStress"]
        self.shearStress /= len(self.data)
        self.Rxz /= len(self.data)
        intphi = integrate(self.zAxis, self.phiPartMean) - primitive(self.zAxis, self.phiPartMean)
        self.gravityTerm = 9.81*np.sin(self.slope)*(self.rhoFluid*(self.zAxis[-1]-self.zAxis) + (self.rhoPart-self.rhoFluid)*intphi)
        Rxz_byHand = self.rhoFluid*(1-self.phiPartMean)*(0.41*primitive(self.zAxis, (0.61-self.phiPartMean)/0.61))**2*np.abs(derivate(self.zAxis, self.vxFluid[:-1]))*derivate(self.zAxis,self.vxFluid[:-1])

        plt.figure()
        plt.plot(self.Rxz, self.zAxis, label=r'$R_ {xz}$')
        plt.plot(self.shearStress, self.zAxis, label=r'$\tau_{xz}$')
        plt.plot(self.gravityTerm, self.zAxis, label=r'Gravity')
        plt.legend()

        plt.figure()
        plt.plot(Rxz_byHand, self.zAxis, label=r'$R_ {xz} by hand$')
        plt.plot(self.shearStress, self.zAxis, label=r'$\tau_{xz}$')
        plt.plot(self.gravityTerm, self.zAxis, label=r'Gravity')
        plt.legend()


if __name__ == '__main__':
    """
	a = ('load_simu', 436, path='/home/remi/Documents/utils/')
	a.read_hdf5()
	a.load_data_dict()
	"""
    a = SimulationYADE('load_simu', path='/home/remi/Documents/utils/')
    a.load_hdf5()
