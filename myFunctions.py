import numpy as np
import os

def find_directory(path, simu):
	"""
	Look for the directory of simu in all the sub directories of path. If several 
	directories are found, the program asks which directory is the good one.
	path : string, reference path where all the simulations are located.
	directory : string, look for this directory in path and sub directories of path  
	"""
	directories = []
	subDirectories = [x[0] for x in os.walk(path)]
	
	for f in subDirectories:
		if f.endswith(simu):
			directories.append(f)
	
	#If no directories found
	if len(directories) == 0:
		raise DirectorySimuError(simu)
	
	#If several directories found, ask for the one wanted
	elif len(directories) > 1:
		print("The following simulations has been found :")
		for i in range(len(directories)):
			print("{} : {}".format(i, directories[i]))
		chosenSimulation = -1
		while (type(chosenSimulation) is not int or
				(chosenSimulation < 0 or chosenSimulation > len(directories)-1)):
			chosenSimulation = int(raw_input("Please, choose one simulation ! (integer between {} and {})"
									.format(0,len(directories)-1)))
		directory = directories[chosenSimulation]
	
	else: directory = directories[0]

	return directory + '/'

def choose_simulation(path):
	"""
	Make a list of all the directories containing a simulation located in path.
	Ask the user which simulation to load
	"""
	directories = []
	subDirectories = [x[0] for x in os.walk(path)]

	for f in subDirectories:
		if f+'/data' in subDirectories:
			directories.append(f)
		elif os.path.isfile(f+'/data.hdf5'):
			directories.append(f)

	#If no directories found
	if len(directories) == 0:
		raise DirectorySimuError(path)
	
	for i in range(len(directories)):
		print("{} : {}".format(i, directories[i]))
	chosenSimulation = -1
	while (type(chosenSimulation) is not int or
			(chosenSimulation < 0 or chosenSimulation > len(directories)-1)):
		chosenSimulation = int(raw_input("Please, choose one simulation ! (integer between {} and {})"
								.format(0,len(directories)-1)))
	directory = directories[chosenSimulation]

	return directory + '/'
	

def find_hdf5_file(path, simu):

	directory = find_directory(path, simu)
	
	h5Files = []
	allFiles = [x[2] for x in os.walk(directory)]
	tmp = []
	for i in range(len(allFiles)):
		tmp += allFiles[i]
	
	allFiles = tmp

	for f in allFiles:
		if f.endswith('.hdf5'):
			h5Files.append(f)
	
	if len(h5Files)==0:
		raise DirectoryH5Error(simu)

	elif len(h5Files) > 1:
		print("The following hdf5 files has been found :")
		for i in range(len(h5Files)):
			print("{} : {}".format(i, h5Files[i]))
		chosenFile=-1
		while (type(chosenFile) is not int or
				(chosenFile < 0 or chosenFile > len(h5Files)-1)):
			chosenFile = int(raw_input("Please, choose one hdf5 file ! (integer between {} and {})"
									.format(0,len(h5Files)-1)))
		h5File = h5Files[chosenFile]
	
	else: h5File = h5Files[0]
	
	return h5File

def find_zero(variable, function):
	"""
	Give the value of variable for which function is zero
	variable : array
	function : array
	variable and function have to be of same size
	"""
	variable = np.array(variable)
	function = np.array(function)
	if len(variable) != len(function):
		raise ValueError("variable and function have to be of same size")
	
	zeros = []
	for i in range(len(function)):
		#The zero is between two values of variable
		if (i<len(function)-1) and (function[i]*function[i+1] < 0.):
			slope = float(function[i+1]-function[i])/float(variable[i+1]-variable[i])
			if abs(slope) < 1e-6: #To avoid division by zero
				zeros.append(variable[i])
			else:
				zeros.append(variable[i] - function[i]/slope)
		#The zero is exactly at one value of variable
		elif function[i] == 0.:
			zeros.append(variable[i])
	
	zeros = np.array(zeros)
	return zeros

def derivate(variable, function):
	"""
	Make the derivation of function with respect to variable.
	Centered derivative except on both bounds where right derivative 
	and left derivative are performed. Thus the output has the same 
	length than function.
	variable : array
	function : array
	variable and function have to be of same size
	"""

	derivFunc = np.zeros(len(function))
	derivFunc[0] = float(function[1]-function[0])/float(variable[1]-variable[0])
	derivFunc[-1] = float(function[-1]-function[-2])/float(variable[-1]-variable[-2])
	for i in range(1,len(function)-1):
		derivFunc[i] =  float(function[i+1]-function[i-1])/float(variable[i+1]-variable[i-1])

	return derivFunc

def integrate(variable, function, dz=None):
    """
    Integrate function as a function of variable.
    Integration by the method of trapeze
    """
    integral = 0.
    for k in range(len(function)-1):
        integral += (variable[k+1]-variable[k])*(function[k+1]+function[k])/2

    return integral

def primitive(variable, function):
    """
    Compute a primitive of function
    """
    primit = [0.0]
    for i in range(1,len(function)):
        primit.append(integrate(variable[:i],function[:i]))

    return np.array(primit)

def primitive_new(variable, function):
    """
    Compute a primitive of function
    """
    primit = [0.0]
    integral = 0.0
    for i in range(1,len(function)):
        integral += (variable[i+1]-variable[i])*(function[i+1]+function[i])/2
        primit.append(integral)

    return np.array(primit)

def Vslice(z1, z2, zc, R):
    """
    Compute the volume of a slice of a sphere.
    z1, z2 : slice between z1 and z2. z1<z2
    zc : vertical position of the center of the sphere
    R : radius of the sphere
    """
    #In the referential of the sphere
    z1 = z1-zc
    z2 = z2-zc

    #-R<=z1<=R and -R<=z2<=R
    z1=min(max(z1,-R),R)
    z2=min(max(z2,-R),R)

    V = np.pi*(R**2*(z2-z1)-1./3*(z2**3-z1**3))
    return V

def lissage(Lx,Ly,p):
    """
    Fonction qui debruite une courbe par une moyenne glissante
    sur 2P+1 points
    """
    Lxout=[]
    Lyout=[]
    Lxout = Lx[p: -p]
    for index in range(p, len(Ly)-p):
        average = np.mean(Ly[index-p : index+p+1])
        Lyout.append(average)
    return Lxout,Lyout
