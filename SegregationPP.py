import matplotlib.pyplot as plt
import scipy
import h5py
from scipy.interpolate import interp1d

from myErrors import *
from myFunctions import *

plt.ion()

path = "/home/remi1/Simulations/"

class SegregationPP(object):
    """
    Sub class of SimulationYADE located in loadSimulation.py.
    Class to make the post-processing of a segregation numerical experiment.
    simu : str, name of the folder in which the experiment is located. If
        not given by the user, makes a list of all simulations and ask the
        one to load.
    path : str, path in which all simulations are located
    start : int, number of the first iteration loaded, 0 if not given by
        the user
    end : int, number of the last iteration loaded, take the last iteration
        of the computation if not given by the user
    step : step of the iteration loaded. If not given, choose an adapted
        step (1 if end-start<100, 10 if end-start<1000, 100 otherwise)
    """
    def __init__(self, simu=None, path=path, start=1, end=None, step=None):
        self.path = path
        if simu==None:
            self.directory = choose_simulation(path)
            self.simu = self.directory.split('/')[-2]
        else:
            self.simu = simu
            self.directory = find_directory(path, simu)
            self.top = 0

    def load_GeomParam(self):
        """
        read the GeomParam.txt file that should be located in the directory
        of the experiment and load the value given. Make also the zAxis vector
        (z/d1 vector) and the time vector.
        Structure of the file (example):
        Nlayer1 = 12
            Nlayer2 = 1
            diameterPart1 = 6e-3
        diameterPart2 = 4e-3
            waterDepth = 7*6e-3
        slope = 0.1
        measurePeriod = 0.5
        """
        myFile = self.directory+'GeomParam.txt'
        with open(myFile,'r') as f:
            for lines in f:
                if lines.startswith('Nlayer1'):
                    self.Nlayer1 = float(lines.split(' ')[-1].split('\n')[0])
                if lines.startswith('Nlayer2'):
                    self.Nlayer2 = float(lines.split(' ')[-1].split('\n')[0])
                if lines.startswith('diameterPart1'):
                    self.diameterPart1 = float(lines.split(' ')[-1].split('\n')[0])
                if lines.startswith('diameterPart2'):
                    self.diameterPart2 = float(lines.split(' ')[-1].split('\n')[0])
                if lines.startswith('waterDepth'):
                    self.waterDepth = eval(lines.split(' ')[-1].split('\n')[0])
                if lines.startswith('slope'):
                    self.slope = float(lines.split(' ')[-1].split('\n')[0])
                if lines.startswith('measurePeriod'):
                    self.measurePeriod = float(lines.split(' ')[-1].split('\n')[0])
        self.fluidHeight = self.Nlayer1*self.diameterPart1 + self.Nlayer2*self.diameterPart2 + self.waterDepth
        self.rhoPart = 2500.
        self.rhoFluid = 1000.

    def find_quasiStatic(self, crit=0.20):
        """
        Find the lower and upper bound of the quasi static part, based on the analysis of vxPartMean
        """
        index = np.where(self.vxPartMean>0.0)
        vx = self.vxPartMean[index]
        z = self.zAxis[index]
        lnvx = np.log(vx)
        dlnvx = derivate(z,lnvx)

        #Compute the index at half height
        middleIndex = int(find_zero(range(len(z)),z-(z[-1]+z[0])/2))
        middleSlope = dlnvx[middleIndex]
        upperBound = middleIndex
        lowerBound = middleIndex
        while (abs(dlnvx[upperBound]-middleSlope) < crit*middleSlope):# and (dlnvx[upperBound]<1.0):
            upperBound +=1
        while (abs(dlnvx[lowerBound]-middleSlope) < crit*middleSlope):# and (dlnvx[lowerBound]<1.0):
            lowerBound -=1
        self.quasiStaticLowerBound = z[lowerBound]
        self.quasiStaticUpperBound = z[upperBound]
        self.zQS = z[lowerBound:upperBound+1]

    def compute_I(self, dmean=False, pmean=False):
        self.IMean = np.zeros(len(self.zAxis))
        if dmean is True:
            self.d_bar = self.phiPartGran1 * self.diameterPart1 + self.phiPartGran2 * self.diameterPart2
        else:
            self.d_bar = self.diameterPart1*np.ones(len(self.zAxis))
        if pmean is True:
            self.pressure = self.PMean
        else:
            self.pressure = np.zeros(len(self.zAxis))
            for i in range(len(self.zAxis)):
                self.pressure[i] = integrate(self.zAxis[i:], self.phiPartMean[i:]) * self.H
            self.pressure *= (self.rhoPart - self.rhoFluid) * 9.81 * np.cos(self.slope)
        index = np.where(self.pressure > 0)
        self.IMean[index] = self.d_bar[index] * self.shearRate[index]/ np.sqrt(self.pressure[index] / self.rhoPart)
        self.IMeanF = interp1d(self.zAxis, self.IMean)

    def find_quasiStatic_I(self, crit=0.20):
        """
        Find the lower and upper bound of the quasi static part, based on the analysis of I
        """
        index = np.where(self.IMean>0.0)
        IMean = self.IMean[index]
        z = self.zAxis[index]
        lnI = np.log(IMean)
        dlnI = derivate(z,lnI)

        #Compute the index of the beginning of the quasi static part
        firstIndex = int(find_zero(range(len(IMean)),IMean-1e-2))
        firstdlnI = dlnI[firstIndex]
        upperBound = firstIndex
        lowerBound = firstIndex
        while (abs(dlnI[lowerBound]-firstdlnI) < crit*firstdlnI):# and (dlnvx[lowerBound]<1.0):
            lowerBound -=1
        self.quasiStaticLowerBound = z[lowerBound]
        self.quasiStaticUpperBound = z[upperBound]
        self.zQS = z[lowerBound:upperBound+1]

    def modify_quasiStatic(self, index):
        """
        This function is used if, at the end of computation, the position of the center of mass
        of fine particles is still in the quasi-static region determined by the function
        find_quasistatic. It modifies the lower boundary of the quasi-static part.
        """
        self.zQS = self.zAxis[index]
        text = "zQS"
        try :
            interpFunction = interp1d(self.massCenter, self.time, fill_value="extrapolate")
            self.timeQS = interpFunction(self.zQS)
            text += ", timeQS"
        except AttributeError:
            pass
        try :
            self.vxQS = self.coefVx[0]*np.exp(self.zQS/self.coefVx[1])
            self.gamma = self.coefShear[0]*np.exp(self.zQS/self.coefShear[1])
            text += ", vxQS and gamma"
        except AttributeError:
            pass
        text += " has(have) been modified"
        print(text)

    def vxPart_fit(self, zStart=None, zEnd=None, plot=True):
        """
        Fit the mean total particle velocity.
        Vx = a*exp(z/b)
        ln(Vx) = ln(a) + z/b
        coefVx = [a, b]
        """
        if zStart==None:
            zStart = max(self.zAxis[0], self.quasiStaticLowerBound)
        else:
            zStart = max(self.zAxis[0], zStart)
        if zEnd==None:
            zEnd = min(self.zAxis[-1], self.quasiStaticUpperBound)
        else:
            zEnd = min(self.zAxis[-1], zEnd)

        index = np.where((self.zAxis>=zStart)&(self.zAxis<=zEnd))[0]
        if self.zQS[0]!= self.zAxis[index][0] or self.zQS[-1]!=self.zAxis[index][-1]:
            print("Warning : the zone corresponding to the quasi static part is being modified.")
            self.modify_quasiStatic(index)
        lnVx = np.log(self.vxPartMean[index])
        lnShearRate = np.log(self.shearRate[index])
        [b, lna] = np.polyfit(self.zAxis[index], lnVx, 1)
        self.coefVx = [np.exp(lna), 1./b]
        [b, lna] = np.polyfit(self.zAxis[index], lnShearRate, 1)
        self.coefShear = [np.exp(lna), 1./b]

        self.zQS = self.zAxis[index]
        self.vxQS = self.coefVx[0]*np.exp(self.zQS/self.coefVx[1])
        self.gamma = self.coefShear[0]*np.exp(self.zQS/self.coefShear[1])

        if plot is True:
            linewidth = 3
            plt.figure()
            plt.subplot(121)
            plt.plot(self.vxPartMean, self.zAxis, linewidth = linewidth)
            plt.plot(self.vxQS, self.zQS, 'r--',
                    label=r'${:.1e} \times exp($'.format(self.coefVx[0])+r'$\bar{z}/$'+r'${:.3f}$'.format(self.coefVx[1])+r'$)$',
                    linewidth=linewidth)
            plt.legend(loc=2, prop={'size':20})
            plt.xlabel(r'$\left<V_x\right>$ $(m.s^{-1})$', fontsize=30)
            plt.ylabel(r'$\bar{z}$', rotation=True, horizontalalignment='right', fontsize=30)
            plt.ylim([self.zAxis[0], self.zAxis[-1]])

            plt.subplot(122)
            plt.semilogx(self.vxPartMean, self.zAxis, linewidth = linewidth)
            plt.semilogx(self.vxQS, self.zQS, 'r--',
                         label=r'${:.3f}$'.format(np.log(self.coefVx[0]))+r'$ + \bar{z}/$'+r'${:.3f}$'.format(self.coefVx[1]),
                         linewidth=linewidth)
            plt.legend(loc=2, prop={'size':20})
            plt.grid(linewidth=2)
            plt.xlabel(r'$\left<V_x\right>$ $(m.s^{-1})$', fontsize=30)
            plt.ylabel(r'$\bar{z}$', rotation=True, horizontalalignment='right', fontsize=30)
            plt.ylim([self.zAxis[0], self.zAxis[-1]])

    def I_fit(self, zStart=None, zEnd=None, plot=True):
        if zStart==None:
            zStart = max(self.zAxis[0], self.quasiStaticLowerBound)
        else:
            zStart = max(self.zAxis[0], zStart)
        if zEnd==None:
            zEnd = min(self.zAxis[-1], self.quasiStaticUpperBound)
        else:
            zEnd = min(self.zAxis[-1], zEnd)

        index = np.where((self.zAxis>=zStart)&(self.zAxis<=zEnd))[0]
        if self.zQS[0]!= self.zAxis[index][0] or self.zQS[-1]!=self.zAxis[index][-1]:
            print("Warning : the zone corresponding to the quasi static part is being modified.")
            self.modify_quasiStatic(index)

        lnI = np.log(self.IMean[index])
        [b, lna] = np.polyfit(self.zAxis[index], lnI, 1)
        self.coefI = [np.exp(lna), 1./b]
        self.zQS = self.zAxis[index]
        self.IQS = self.coefI[0]*np.exp(self.zQS/self.coefI[1])

        if plot is True:
            plt.figure()
            linewidth=3
            plt.semilogx(self.IMean, self.zAxis, linewidth=linewidth)
            plt.semilogx(self.IQS, self.zQS, 'r--',
                        label=r'${:.3f}$'.format(np.log(self.coefI[0]))+r'$ + \bar{z}/$'+r'${:.3f}$'.format(self.coefI[1]),
                        linewidth=linewidth)
            plt.legend(loc=2, prop={'size':20})
            plt.grid(linewidth=2)
            plt.xlabel(r'$I$', fontsize=30)
            plt.ylabel(r'$\bar{z}$', rotation=True, horizontalalignment='right', fontsize=30)
            plt.ylim([self.zAxis[0], self.zAxis[-1]])

    def ZmassCenter(self, zStart=None, zEnd=None, crit=0.05, plot=True):
        """
        Make a logarithmic fit of the position of the center of mass.
        zStart : float, first time of the fitting.
            If None, make the fit from the beginning of the quasi static part
        zEnd : float, last time of the fitting
            If None, make the fit until the end of the quasi static part
        """
        if zStart==None:
            zStart = max(self.massCenter[-1], self.quasiStaticLowerBound)
        else:
            zStart = max(self.massCenter[-1], zStart)
        if zEnd==None:
            zEnd = min(self.massCenter[0], self.quasiStaticUpperBound)
        else:
            zEnd = min(self.massCenter[0], zEnd)

        index = np.where((self.zAxis>=zStart)&(self.zAxis<=zEnd))[0]
        if self.zQS[0]!= self.zAxis[index][0] or self.zQS[-1]!=self.zAxis[index][-1]:
            print("Warning : the zone corresponding to the quasi static part is being modified.")
            self.modify_quasiStatic(index)

        #1d interpolation : This allows during the fit to give same weight to the whole part of the fitting zone
        interpFunction = interp1d(self.massCenter, self.time)
        self.timeQS = interpFunction(self.zQS)

        # Logarithmic fit of the position of the center of mass
        # z = b + a ln(t)
        logTime = np.log(self.timeQS)
        [a_mc, b_mc] = np.polyfit(logTime, self.zQS, 1)
        self.coefMassCenter = [a_mc, b_mc]

        # Computation of dzdt
        self.dzdt = derivate(self.time, self.massCenter)
        if plot is True:
            plt.figure()
            linewidth=3
            plt.subplot(2,1,1)
            plt.plot(self.time, self.massCenter, 'b', linewidth=linewidth)
            plt.plot(self.timeQS, b_mc + a_mc*np.log(self.timeQS), 'r--',
                     label=r'${:.3f}\ln(t) + {:.3f}$'.format(a_mc,b_mc), linewidth=linewidth)
            plt.legend(prop={'size':20})
            plt.xlabel(r'$t$', fontsize=24)
            plt.ylabel(r'$\bar{z}$', rotation=True, horizontalalignment='right', fontsize=24)
            plt.ylim([self.zAxis[0],self.zAxis[-1]])

            plt.subplot(2,1,2)
            plt.semilogx(self.time, self.massCenter, 'b', linewidth=linewidth)
            plt.semilogx(self.timeQS, b_mc + a_mc*np.log(self.timeQS), 'r--',
                label=r'${:.3f}\ln(t) + {:.3f}$'.format(a_mc,b_mc), linewidth=linewidth)
            plt.legend(prop={'size':20})
            plt.grid()
            plt.xlabel(r'$t$', fontsize=24)
            plt.ylabel(r'$\bar{z}$', rotation=True, horizontalalignment='right', fontsize=24)

    def segregation_flux(self, zStart=None, zEnd=None, layerWidth=None,
                         plot=True, withGamma=False, withI=True):

        if zStart==None:
            zStart = max(self.massCenter[-1], self.quasiStaticLowerBound)
        else:
            zStart = max(self.massCenter[-1], zStart)
        if zEnd==None:
            zEnd = min(self.massCenter[0], self.quasiStaticUpperBound)
        else:
            zEnd = min(self.massCenter[0], zEnd)

        index = np.where((self.zAxis>=zStart)&(self.zAxis<=zEnd))[0]
        if self.zQS[0]!= self.zAxis[index][0] or self.zQS[-1]!=self.zAxis[index][-1]:
            print("Warning : the zone corresponding to the quasi static part is being modified.")
            self.modify_quasiStatic(index)

        if layerWidth is None:
            layerWidth = self.Nlayer2*self.diameterPart2/self.H
        indexMC = np.where((self.massCenter>=zStart)&(self.massCenter<=zEnd))

        time = self.time[indexMC]
        dzdt_simu = self.dzdt[indexMC]
        z_simu = self.massCenter[indexMC]
        self.dzdt_fit = self.coefMassCenter[0]/self.timeQS
        z = self.coefMassCenter[0]*np.log(self.timeQS)+self.coefMassCenter[1]
        if withGamma:
            self.gammaQS = self.shearRateF(self.zQS)
            self.gammaBot = self.shearRateF(self.zQS - layerWidth)
            [a, b] = np.polyfit(self.gammaQS, self.dzdt_fit, 1)
            [a2, b2] = np.polyfit(self.gammaBot, self.dzdt_fit, 1)
            self.coefgammaQS = [a,b]
            self.coefgammaBot = [a2,b2]
            self.gammaTh = [-np.exp(-self.coefVx[1]*self.coefMassCenter[1])/(self.coefVx[0]*self.coefVx[1]**2), \
                            -self.coefMassCenter[0]**2*np.exp(self.coefMassCenter[1]/self.coefMassCenter[0])/self.coefVx[0]]

        if withI:
            self.IQS = self.IMeanF(self.zQS)
            self.IBot = self.IMeanF(self.zQS-layerWidth)
            coefFlux = np.polyfit(np.log(self.IBot),
                                  np.log(-self.dzdt_fit), 1)
            print(r'$dz/dt = {}I^{}$'.format(np.exp(coefFlux[1]),
                                             coefFlux[0]))
            self.Sr0 = np.exp(coefFlux[1])
            self.powerI = coefFlux[0]

        if plot is True:
            linewidth=3
            plt.figure()

            if withGamma:
                plt.loglog(self.gammaQS, -self.dzdt_fit, '+')
                plt.loglog(self.gammaQS, -(self.coefgammaQS[0]*(self.gammaQS) +
                                           self.coefgammaQS[1]),
                    'r--', label=r'${:.3f}{} + {:.3f}$'.format(self.coefgammaQS[0], "\dot{\gamma}", self.coefgammaQS[1]),
                    linewidth=linewidth)
                plt.plot(self.gammaBot, self.dzdt_fit, '+')
                plt.plot(self.gammaBot, self.coefgammaBot[0]*(self.gammaBot) + self.coefgammaBot[1],
                         '--', label=r'${:.3f}{} + {:.3f}$'.format(self.coefgammaBot[0], "\dot{\gamma}_{bot}", self.coefgammaBot[1]),
                         linewidth=linewidth)
                plt.xlabel(r'$\dot{\gamma}$')
            if withI:
                plt.loglog(self.IQS, -self.dzdt_fit, '+', label=r'$I$')
                plt.loglog(self.IBot, -self.dzdt_fit, '+', label=r'$I_b$')
                plt.loglog(self.IBot, self.Sr0*self.IBot**(self.powerI), 'k--',
                           label=r'${}I_b^{}$'.format(self.Sr0, self.powerI))
                plt.xlabel(r'$I$', fontsize=25)
            plt.ylabel(r'$|\frac{dz}{dt}|$', rotation=True,
                       horizontalalignment='right', fontsize=25)
            plt.legend()
            plt.grid()

    def write_data(self, varName, fileName, varValue=None, overwrite=0):
        """
        Save in the .hdf5 file fileName, the variable varName.
        varName : str, name of the variable
        varValue : array containing the values of varName
        fileName : str, name of the file in which the variable will be saved. Has to be a .hdf5 file.
        """
        if varValue is None:
            try:
                varValue = self.__getattribute__(varName)
            except AttributeError:
                print("Variable {} does not exist.".format(varName))
                return
        fileName = self.directory+'/'+fileName
        h5file = h5py.File(fileName, 'a')
        try:
            grp = h5file.create_dataset(name=varName, data=varValue)
        except RuntimeError:
            if overwrite==1:
                del h5file[varName]
                grp = h5file.create_dataset(name=varName, data=varValue)
                print("Variable with name {} has been overwritten".format(varName))
            else:
                print("Variable with name {} already exists in {}".format(varName, fileName))

    def save_average_values(self, fileName='average.hdf5', overwrite=0):
        self.write_data("time", fileName, overwrite=overwrite)
        self.write_data("qs", fileName, overwrite=overwrite)
        self.write_data("massCenter", fileName, overwrite=overwrite)
        self.write_data("zAxis", fileName, overwrite=overwrite)
        self.write_data("vxPartMean", fileName, overwrite=overwrite)
        self.write_data("vxPartMean1", fileName, overwrite=overwrite)
        self.write_data("vxPartMean2", fileName, overwrite=overwrite)
        self.write_data("phiPartMean", fileName, overwrite=overwrite)
        self.write_data("phiPartMean1", fileName, overwrite=overwrite)
        self.write_data("phiPartMean2", fileName, overwrite=overwrite)
        self.write_data("phiPartGran1", fileName, overwrite=overwrite)
        self.write_data("phiPartGran2", fileName, overwrite=overwrite)
        self.write_data("turbStress", fileName, overwrite=overwrite)
        self.write_data("etaMean", fileName, overwrite=overwrite)
        self.write_data("muMean", fileName, overwrite=overwrite)
        self.write_data("tauMean", fileName, overwrite=overwrite)
        self.write_data("PMean", fileName, overwrite=overwrite)
        self.write_data("etaMean1", fileName, overwrite=overwrite)
        self.write_data("tauMean1", fileName, overwrite=overwrite)
        self.write_data("PMean1", fileName, overwrite=overwrite)
        self.write_data("etaMean2", fileName, overwrite=overwrite)
        self.write_data("tauMean2", fileName, overwrite=overwrite)
        self.write_data("PMean2", fileName, overwrite=overwrite)

    def zero_top(self, plot=False):
        if self.top==0:
            self.zAxis -= (self.Nlayer1*self.diameterPart1 + self.Nlayer2*self.diameterPart2)/self.H
            self.massCenter -= (self.Nlayer1*self.diameterPart1 + self.Nlayer2*self.diameterPart2)/self.H
            self.compute_dynamics()
            #self.compute_I()
            self.find_quasiStatic()
            self.vxPart_fit(plot=plot)
            self.ZmassCenter(plot=plot)
            self.segregation_flux(plot=plot)
            self.top = 1
        else:
            print("Zero is already at the top")

    def zero_down(self, plot=False):
        if self.top==1:
            self.zAxis += (self.Nlayer1*self.diameterPart1 + self.Nlayer2*self.diameterPart2)/self.H
            self.massCenter += (self.Nlayer1*self.diameterPart1 + self.Nlayer2*self.diameterPart2)/self.H
            self.compute_dynamics()
            self.find_quasiStatic()
            self.vxPart_fit(plot=plot)
            self.ZmassCenter(plot=plot)
            self.segregation_flux(plot=plot)
            self.top = 0
        else:
            print("Zero is already at the bottom")
