import numpy as np
import matplotlib.pyplot as plt
import os
import h5py
from time import time

from myErrors import *
from myFunctions import *
from SegFull import *

plt.ion()

path = "/home/remi1/Simulations/"

class SegDry(SegFull):
    """
    TODO : 
    - load_velocity pas adapter au cas sec

    """
    def __init__(self, simu=None, path=path, start=0, end=None, step=None):
        """
    path : str, reference path where all simulations are stored
    simu : str, name of the folder in which the simulation to load is located
    start : int, first time step to load
        end : int, last time step to load. If None takes the last time step of the .hdf5 file
        step : int, step of loading. If None, take an appropriate one
    """
        SegregationPP.__init__(self, simu, path)

        # self.hdf5File = find_hdf5_file(self.path, self.simu)
        self.H = 1
        self.T = 1
        self.hdf5File = "data.hdf5"
        self.load_hdf5(start=start, end=end, step=step)
        self.load_GeomParam()
        self.axis()
        self.create_phiPartGran()
        self.particle_averaged()
        self.compute_dynamics()
        #self.compute_I()
        #self.find_quasiStatic()
        #self.load_velocity()
        #self.percolation_velocity()

    
    def load_GeomParam(self):
        myFile = self.directory+'GeomParam.txt'
        with open(myFile,'r') as f:
            for lines in f:
                if lines.startswith('Nlayer1'):
                    self.Nlayer1 = float(lines.split(' ')[-1].split('\n')[0])
                if lines.startswith('Nlayer2'):
                    self.Nlayer2 = float(lines.split(' ')[-1].split('\n')[0])
                if lines.startswith('diameterPart1'):
                    self.diameterPart1 = float(lines.split(' ')[-1].split('\n')[0])
                if lines.startswith('diameterPart2'):
                    self.diameterPart2 = float(lines.split(' ')[-1].split('\n')[0])
                if lines.startswith('slope'):
                    self.slope = float(lines.split(' ')[-1].split('\n')[0])
                if lines.startswith('measurePeriod'):
                     self.measurePeriod = float(lines.split(' ')[-1].split('\n')[0])

        self.Height = self.Nlayer1*self.diameterPart1+(self.Nlayer2+10)*self.diameterPart2
        self.rhoPart = 2500.
    def axis(self):
        self.time = np.linspace(self.firstTimeStepLoaded*self.measurePeriod, self.lastTimeStepLoaded*self.measurePeriod,len(self.data))/self.T
        self.zAxis = self.data[0].var['zAxis']
        self.dz = self.zAxis[1]-self.zAxis[0]
        self.ndimz = len(self.zAxis)

    def create_phiPartGran(self):
        """
        Compute the volume fraction of large and small particles per unit granular volume. 
        """
        for i in range(len(self.data)):
            self.data[i].var["phiPart"] = self.data[i].var["phiPart1"]+self.data[i].var["phiPart2"]
            phiPartGran1 = np.zeros(len(self.data[0].var["phiPart"]))
            phiPartGran2 = np.zeros(len(self.data[0].var["phiPart"]))
            for j in range(len(phiPartGran1)):
                if self.data[i].var["phiPart"][j] > 1.e-6:
                    phiPartGran1[j] = self.data[i].var["phiPart1"][j]/self.data[i].var["phiPart"][j]
                    phiPartGran2[j] = self.data[i].var["phiPart2"][j]/self.data[i].var["phiPart"][j]
            self.data[i].var["phiPartGran1"] = phiPartGran1
            self.data[i].var["phiPartGran2"] = phiPartGran2

    def particle_averaged(self):
        """
        Compute the some mean values related to the particles (small and large together) :
        - total particles velocity : vxPartTot (not averaged).
        - mean particles velocity : vxPartMean (size ndimz).
        - mean particles concentration : phiPartMean (size ndimz).
        - mean solid shear stress
        """
        vxPartMean = np.zeros(self.ndimz)
        phiPartMean = np.zeros(self.ndimz)
        vxPartMean1 = np.zeros(self.ndimz)
        phiPartMean1 = np.zeros(self.ndimz)
        phiPartGran1 = np.zeros(self.ndimz)
        vxPartMean2 = np.zeros(self.ndimz)
        phiPartMean2 = np.zeros(self.ndimz)
        phiPartGran2 = np.zeros(self.ndimz)
        qs = np.zeros(len(self.data))

        for i in range(len(self.data)):
            self.data[i].var["vxPartTot"] = (self.data[i].var["vxPart1"]*self.data[i].var["phiPartGran1"] + \
                        self.data[i].var["vxPart2"]*self.data[i].var["phiPartGran2"]) 
            phiPartMean += (self.data[i].var["phiPart1"] + self.data[i].var["phiPart2"])
            phiPartMean1 += self.data[i].var["phiPart1"]
            phiPartMean2 += self.data[i].var["phiPart2"]
            phiPartGran1 += self.data[i].var["phiPartGran1"]
            phiPartGran2 += self.data[i].var["phiPartGran2"]
            vxPartMean1 += self.data[i].var["phiPart1"]*self.data[i].var["vxPart1"]
            vxPartMean2 += self.data[i].var["phiPart2"]*self.data[i].var["vxPart2"]
            vxPartMean += (self.data[i].var["phiPart1"] + self.data[i].var["phiPart2"]) * self.data[i].var["vxPartTot"]
            self.data[i].var["shearStress"] = derivate(self.zAxis, self.data[i].var["vxPartTot"])
            qs[i] = self.dz*sum(self.data[i].var["vxPartTot"]*self.data[i].var["phiPart"])

        nonZeroIndices = np.where(phiPartMean>0)
        vxPartMean[nonZeroIndices] = vxPartMean[nonZeroIndices]/phiPartMean[nonZeroIndices]
        self.vxPartMean = vxPartMean
        nonZeroIndices = np.where(phiPartMean1>0)
        vxPartMean1[nonZeroIndices] = vxPartMean1[nonZeroIndices]/phiPartMean1[nonZeroIndices]
        self.vxPartMean1 = vxPartMean1
        nonZeroIndices = np.where(phiPartMean2>0)
        vxPartMean2[nonZeroIndices] = vxPartMean2[nonZeroIndices]/phiPartMean2[nonZeroIndices]
        self.vxPartMean2 = vxPartMean2
        self.phiPartMean = phiPartMean/len(self.data)
        self.phiPartMean1 = phiPartMean1/len(self.data)
        self.phiPartMean2 = phiPartMean2/len(self.data)
        self.phiPartGran1 = phiPartGran1/len(self.data)
        self.phiPartGran2 = phiPartGran2/len(self.data)
        self.qs = qs


