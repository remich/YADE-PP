import h5py
import numpy as np
import os
import sys

global lines 
lines = []

class dataToH5(object):
	
    def __init__(self, pythonFile, path):
	self.fileToLoad = path+'/'+pythonFile
	
    def get_variables(self):
	"""
	Read the python file and stores the name of the variables saved in this file
	"""
	varName = []

	with open(self.fileToLoad,'r') as f:
	    for line in f:
                lines.append(line)
		try :
		    float(line.split()[0][0:2])
		except ValueError:
		    if line.split()[0] != 'from':
			varName.append(line.split()[0])
	self.varName = varName

    def load_file(self):
	self.get_variables()
	varValue = []
	execfile(self.fileToLoad)
	for i in range(len(self.varName)):
    	    if type(eval(self.varName[i])) is not np.ndarray :
		varValue.append(np.array([eval(self.varName[i])]))
	    else:
		varValue.append(eval(self.varName[i]))
	self.varValue = varValue

    def write_hdf5(self, fileToSave=None):
	path = os.getcwd()
	if fileToSave==None:
    	    fileToSave = self.fileToLoad.split('/')[-1].split('.')[0]
	fileToSave = path + '/' + fileToSave + '.hdf5'

    	h5file = h5py.File(fileToSave, 'a')
    	try :
	    grp = h5file.create_group(self.fileToLoad.split('/')[-1].split('.')[0])
	except ValueError:
	    print("Warning : a group named {}\r already exists, ".format(self.fileToLoad.split('/')[-1].split('.')[0]) +\
				"the program did not overwrite it"),
            sys.stdout.flush()
	else:
    	    for i in range(len(self.varName)):
		dataSet = grp.create_dataset(name=self.varName[i], data=self.varValue[i])
	h5file.close()

    def write_hdf5_stress(self, fileToSave=None):
        path = os.getcwd()
	if fileToSave==None:
	    fileToSave = self.fileToLoad.split('/')[-1].split('.')[0]
	fileToSave = path + '/' + fileToSave + '.hdf5'
        h5file = h5py.File(fileToSave, 'a')
    	try :
	    grp = h5file.create_group(self.fileToLoad.split('/')[-1].split('.')[0])
	except ValueError:
	    print("Warning : a group named {}\r already exists, ".format(self.fileToLoad.split('/')[-1].split('.')[0]) +\
				"the program did not overwrite it"),
            sys.stdout.flush()
	else:
            #for i in range(len(self.varName)):
		#dataSet = grp.create_dataset(name=self.varName[i], data=self.varValue[i][0][0])
            
            pressure = np.zeros(len(self.varValue[0][0][0]))
            for i in range(len(self.varValue[0][0][0])):
                stressProfile = self.varValue[0][0][0][i]
                pressure[i] = -1./3*(stressProfile[0]+stressProfile[4]+stressProfile[8])
            dataSet = grp.create_dataset(name='pressure', data=pressure)
        

class translateData(object):

    def __init__(self):
        self.path = os.getcwd()

    def write_data(self, fileToSave='data.hdf5'):
	data_path = self.path + '/data'
	listOfFiles = os.listdir(data_path)
	for i in range(len(listOfFiles)):
	    tmpObj = dataToH5(listOfFiles[i], data_path)
	    tmpObj.load_file()
	    tmpObj.write_hdf5(fileToSave=fileToSave)

    def write_velocity(self, fileToSave='velocity.hdf5'):
        vel_path = self.path + '/velocity'
        listOfFiles = os.listdir(vel_path)
        for i in range(len(listOfFiles)):
            tmpObj = dataToH5(listOfFiles[i], vel_path)
	    tmpObj.load_file()
	    tmpObj.write_hdf5(fileToSave=fileToSave)

    def write_pressure(self, fileToSave='pressure.hdf5'):
        stress_path = self.path + '/stress'
        listOfFiles = os.listdir(stress_path)
        for i in range(len(listOfFiles)):
            tmpObj = dataToH5(listOfFiles[i], stress_path)
            tmpObj.load_file()
            tmpObj.write_hdf5_stress(fileToSave=fileToSave)

    def write_all(self):
        self.write_data()
        self.write_velocity()


if __name__=='__main__':
	a = translateData()
	a.write_hdf5()
        a.write_velocity()

