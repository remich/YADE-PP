class Error(Exception):
	pass

class DirectorySimuError(Error):
	def __init__(self, simu):
		super(DirectorySimuError,self).__init__(
			"No directory found for simulation named {}".format(simu))

class NoSimuError(Error):
	def __init__(self, path):
		super(NoSimuError,self).__init__(
			"No simulation found in path named {}".format(path))

class DirectoryH5Error(Error):
	def __init__(self, simu):
		super(DirectoryH5Error,self).__init__(
			"No hdf5 file found for simulation named {}".format(simu))
	
